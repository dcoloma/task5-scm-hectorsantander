QUnit.test("Boundary Test", function( assert ) {
    assert.equal(isValidPlate('123DD'), false, 'Plate with less than 7 characters');
});

QUnit.test("Letters in the numbers positions.", function( assert ) {
    assert.equal(isValidPlate('Z123GHJ'), false, 'A letter in the first position');
    assert.equal(isValidPlate('4B56DFG'), false, 'A letter in the second position');
    assert.equal(isValidPlate('67C8SDF'), false, 'A letter in the third position');
    assert.equal(isValidPlate('902SKLP'), false, 'A letter in the fourth position')
});

QUnit.test("Number in the letters positions.", function( assert ) {
    assert.equal(isValidPlate('01237SD'), false, 'A number in the fifth position');
    assert.equal(isValidPlate('4567C5K'), false, 'A number in the sixth position');
    assert.equal(isValidPlate('8901HY9'), false, 'A number in the seventh position');
});

QUnit.test("Invalid char in any position", function( assert ) {
    assert.equal(isValidPlate('!123GHJ'), false, 'An invalid char (other characters) in the first position');
    assert.equal(isValidPlate('4$56DFG'), false, 'An invalid char (other characters) in the second position');
    assert.equal(isValidPlate('67&8SDF'), false, 'An invalid char (other characters) in the third position');
    assert.equal(isValidPlate('902?KLP'), false, 'An invalid char (other characters) in the fourth position');
    assert.equal(isValidPlate('0123%SD'), false, 'An invalid char (other characters) in the fifth position');
    assert.equal(isValidPlate('4567C*K'), false, 'An invalid char (other characters) in the sixth position');
    assert.equal(isValidPlate('8901HY#'), false, 'An invalid char (other characters) in the seventh position');
});

QUnit.test("Invalid letter Ñ, Q or Vowel in the letters positions.", function( assert ) {
    assert.equal(isValidPlate('1234ÑFG'), false, 'Ñ, Q or Vowel in fifth position');
    assert.equal(isValidPlate('5678IQJ'), false, 'Ñ, Q or Vowel in sixth position');
    assert.equal(isValidPlate('9012SDA'), false, 'Ñ, Q or Vowel in seventh position');
});

QUnit.test("Valid Lower Case letters", function( assert ) {
    assert.equal(isValidPlate('1234kPL'), true, 'Lower Case letter in fifth position');
    assert.equal(isValidPlate('5678YgJ'), true, 'Lower Case letter in sixth position');
    assert.equal(isValidPlate('9012SDs'), true, 'Lower Case letter in seventh position');
});

QUnit.test("All valid number combinations.", function( assert ) {
    assert.equal(isValidPlate('0123SDF'), true, 'All digits are different');
    assert.equal(isValidPlate('5675GHJ'), true, 'Two digits are equals');
    assert.equal(isValidPlate('8889RTY'), true, 'Three digits are equals');
    assert.equal(isValidPlate('0000PLM'), true, 'Four digits are equals');
});

QUnit.test("All valid letters combinations", function( assert ) {
    assert.equal(isValidPlate('1234TYD'), true, 'All letters are different');
    assert.equal(isValidPlate('4567BFB'), true, 'Two letters are equals');
    assert.equal(isValidPlate('8390DDD'), true, 'Three letters are equals');
});


